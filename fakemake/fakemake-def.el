;;;  -*- lexical-binding: t -*-

(defconst-with-prefix fakemake
  feature 'org-nutrition
  authors "Dima Akater"
  first-publication-year-as-string "2020"
  org-files-in-order `( "org-nutrition-core"
                        "org-nutrition-mode"
                        "org-nutrition"
                        ,@(unless (memq 'minimial use-flags)
                            `("org-nutrition-hello")))
  site-lisp-config-prefix "50"
  license "GPL-3")

(advice-add 'fakemake-prepare :before
            (lambda ()
              (when (memq 'minimal use-flags)
                (delete-file "org-nutrition-hello.org")
                (with-current-buffer (find-file-noselect "org-nutrition.org")
                  (save-excursion
                    (goto-char (point-min))
                    (goto-char
                     (org-find-exact-headline-in-buffer "Introductory command"
                                                        nil t))
                    (delete-region (point)
                                   (let ((section-data
                                          (org-element-section-parser nil)))
                                     (cl-assert (eq 'section
                                                    (car section-data)))
                                     (let ((end (plist-get (cadr section-data)
                                                           :end)))
                                       (cl-check-type end (or integer marker))
                                       (cl-assert (< end (point-max)))
                                       end))))
                  (save-buffer)
                  (kill-buffer)))))

(advice-add 'fakemake-test :before
            (lambda ()
              (require 'akater-misc-buffers-macs)))
