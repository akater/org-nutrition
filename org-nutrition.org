# -*- coding: utf-8; mode: org-development-elisp; -*-
#+title: org-nutrition
#+subtitle: “Main file” for =org-nutrition= Emacs Lisp package
#+author: =#<PERSON akater A24961DE3ADD04E057ADCF4599555CE6F2E1B21D>=
#+property: header-args :tangle org-nutrition.el :lexical t
#+startup: nologdone show2levels
#+todo: TODO(t@) HOLD(h@/!) | DONE(d@)
#+todo: BUG(b@/!) | FIXED(x@)
#+todo: TEST(u) TEST-FAILED(f) | TEST-PASSED(p)
#+todo: DEPRECATED(r@) | OBSOLETE(o@)

=org-nutrition= allows tracking one's nutrition in Org mode.

Nutrition is tracked in an org file structured as datetree:
- top level (=*=) corresponds to years
- second level (=**=) corresponds to months
- third level (=***=) corresponds to days
- deeper levels correspond to units of consumed foods

Yet deeper levels, when they exist, correspond to components of compound foods.

Monthly summary tables are set up to calculate BMR, total nutritional intake, PFC distribution.

* Usage
** Introductory command
For quick start, run =M-x org-nutrition-hello=.

You will see a sample buffer that is supposed to be self-explanatory.  Explore.

** Portions and products
In a org buffer, try =M-x org-nutrition-insert-portion=.  “Inserting a portion” means inserting a pre-recorded meal while “inserting adjusted product” means inserting a pre-recorded sample food of default mass 100g, and adjusting its mass and nutrition according to user-specified amount.  Try =M-x org-nutrition-insert-adjusted-product=, select any product and provide =150g= for the amount.

A depth-4 headline will be inserted, as portions are presumed to be inserted into a [[info:org#Template elements][datetree]] with years, months, days at the top 3 levels.

Meals can be Org entries consisting of multiple subentries (possible natural names for such are “compound foods”, or “recipes”) but working with such entries is not implemented yet.

** Miscellanous
To see up-to-date statistics for the current month, =M-x org-nutrition-goto-monthly-table-and-refresh-it=.  Please note: refresh takes noticeable time at the end of the month, at least with byte-compiled Elisp.

* Installation
** Gentoo
To install, please
#+begin_src sh :dir /sudo::/ :tangle no :results none
eselect repository enable akater && \
emerge app-emacs/org-nutrition
#+end_src

** Other systems
*** TODO Use package-vc (Emacs 29+)
- State "TODO"       from              [2023-06-27 Tue 12:45]
*** Install manually
Ensure the following dependencies are installed into your system-wide site-lisp dir:
- [[https://framagit.org/akater/elisp-mmxx-macros][mmxx-macros]] (only necessary at build time)
- [[https://gitlab.com/akater/elisp-akater-misc][akater-misc]]

Clone ~org-nutrition~, switch to =release= branch.

~org-nutrition~ can be installed into system-wide directory =/usr/share/emacs/site-lisp= (or wherever your ~SITELISP~ envvar points to) with
#+begin_src sh :tangle no :results none
make && sudo make install
#+end_src
but it's best to define a package for your package manager to do this instead.

If your package manager can't do that, and you don't want to install with elevated permissions, do
#+begin_src sh :tangle no :results none
SITELISP=~/.config/emacs/lisp SITEETC=~/.config/emacs/etc make && \
DESTDIR=~/.config/emacs SITELISP=/lisp SITEETC=/etc make install
#+end_src
(you may simply eval this block with =C-c C-c=; it won't be informative if it fails but hopefully it'll work)

In any case, you'll have to add load-path to your config, as demonstrated in [[Suggested config]].

If you don't have a system-wide site-lisp directory where dependencies are found in versionless directories, you may evaluate [[elisp:(url-retrieve"https://framagit.org/akater/emacs-fakemake/-/raw/master/fakemake-make.org"(lambda(_ b &rest __)(eww-parse-headers)(let((rb (current-buffer))(start(point)))(with-current-buffer b(let((inhibit-read-only t))(erase-buffer)(insert-buffer-substring rb start)(goto-char(point-min))(eww-display-raw b'utf-8)(org-mode))(goto-char(org-babel-find-named-block"defun bootstrap-sitelisp"))(pop-to-buffer b)(recenter 1))))(list(get-buffer-create"https+org")))][this block]] and use the command defined there to create a symlink to org (and other packages that you use) in “site-lisp directory to fill” which you may specify.  That directory should be specified as ~SITELISP~ then.  If the link above is scary, find bootstrap-sitelisp manually [[https://framagit.org/akater/emacs-fakemake/-/raw/master/fakemake-make.org][here]].

** Notes for contiributors
If you have a local version of repository, compile with something like
#+begin_example sh :tangle no :results none
ORG_LOCAL_SOURCES_DIRECTORY="/home/contributor/src/org-nutrition"
#+end_example

This will link function definitions to your local Org files.

** Tests
If you want to run tests for ~org-nutrition~, ensure the following dependency is installed:
- [[https://framagit.org/akater/org-run-tests][ORT]]

* Suggested config
#+begin_example elisp
(use-package org-nutrition :ensure nil
  ;; Note: on Gentoo, no need to specify load-path
  :load-path "/usr/share/emacs/site-lisp/org-nutrition"
  ;; or maybe
  ;; :load-path "~/.config/emacs/lisp/org-nutrition"
  :commands ( org-nutrition-insert-portion insert-portion
              org-nutrition-insert-adjusted-product insert-adjusted-product
              org-nutrition-mode)
  :preface
  (defalias 'insert-portion 'org-nutrition-insert-portion)
  (defalias 'insert-adjusted-product 'org-nutrition-insert-adjusted-product)
  :custom
  (org-nutrition-data-filename (expand-file-name
                                "org-nutrition/nutrition-data.org"
                                user-emacs-directory))
  (org-nutrition-default-nutrition-filename (expand-file-name
                                             "org-nutrition/nutrition.org"
                                             user-emacs-directory))
  :mode (("/nutrition\\.org$" . org-nutrition-mode)))
#+end_example

* Features
#+begin_src elisp :results none
(require 'org-nutrition-core)
(require 'org-nutrition-mode)
#+end_src

