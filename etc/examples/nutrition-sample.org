# -*- coding: utf-8; mode: org-nutrition; org-nutrition-birthdate: "1988-01-01"; org-nutrition-height-cm: 175 -*-
#+title: Nutrition Diary

* Local medical information            :noexport:
#+begin_src emacs-lisp :exports results :results none
(defvar-local org-nutrition-birthdate "1988-01-01")
(defvar-local org-nutrition-height-cm 175)
#+end_src

* 2018
:PROPERTIES:
:UNNUMBERED: t
:END:
** 2018-11
:PROPERTIES:
:COLUMNS:  14%ITEM(Date&foods) %protein(Prt, g){+;%.1f} %fat(Fat, g){+;%.1f} %carbs(Crb, g){+;%.1f} %kcal(E, Kc){+;%.0f} %bmr(BMR, Kc) %my-weight(WT, kg){mean;%.2f} %prt(Prt){mean;%.0f} %prt(Fat){mean;%.0f} %prt(Crb){mean;%.0f}
:END:
#+BEGIN: columnview :hlines 1 :id local :maxlevel 3
| Date&foods | Prt, g | Fat, g | Crb, g | E, Kc | BMR, Kc | WT, kg | Prt | Fat | Crb |
|------------+--------+--------+--------+-------+---------+--------+-----+-----+-----|
|    2018-11 |   85.5 |  104.0 |  111.3 |  1733 |    1803 |  78.25 |  28 |  35 |  37 |
| 2018-11-08 |   85.5 |  104.0 |  111.3 |  1733 |    1803 |  78.25 |  28 |  35 |  37 |
#+TBLFM: $6='(org-nutrition-table-bmr $7 $1);%.0f::@2$2=vmean(@<<<$0..@>$0);%.1f::@2$3=vmean(@<<<$0..@>$0);%.1f::@2$4=vmean(@<<<$0..@>$0);%.1f::@2$5=vmean(@<<<$0..@>$0);%.0f::@2$6=vmean(@<<<$0..@>$0);%.0f::@2$7=vmean(@<<<$0..@>$0);%.2f::$8=100*$2/($2+$3+$4);%.0f::$9=100*$3/($2+$3+$4);%.0f::$10=100*$4/($2+$3+$4);%.0f
#+END:
*** 2018-11-08
:PROPERTIES:
:COLUMNS:  14%ITEM(Date&foods) %protein(Prt, g){+;%.1f} %fat(Fat, g){+;%.1f} %carbs(Crb, g){+;%.1f} %kcal(Kcal){+;%.0f} %my-weight(WT , g){+;%.2f}
:my-weight:  78.25
:END:
**** pork schnitzel, 4
:PROPERTIES:
:mass:     100
:protein:  19
:fat:      32.2
:carbs:    8.8
:kcal:     403
:END:
**** onigiri (sockeye), 1
:PROPERTIES:
:mass:     100
:protein:  6.5
:fat:      4.8
:carbs:    27.4
:kcal:     180
:END:
**** burger My Home Burger, 1
:PROPERTIES:
:COLUMNS:  14%ITEM(Foods) %protein(Prt, g){+;%.1f} %fat(Fat, g){+;%.1f} %carbs(Crb, g){+;%.1f} %kcal(E, Kc){+;%.0f} %mass(WT, g){+;%.0f} %prt(Prt){mean;%.0f} %prt(Fat){mean;%.0f} %prt(Crb){mean;%.0f}
:END:
#+BEGIN: columnview :hlines 1 :id local :maxlevel 5
| Foods                                 | Prt, g | Fat, g | Crb, g | E, Kc | WT, g | Prt | Fat | Crb |
|---------------------------------------+--------+--------+--------+-------+-------+-----+-----+-----|
| burger My Home Burger, 1              |   30.1 |   44.0 |   51.8 |   723 |   287 |  24 |  35 |  41 |
| bun Example Brand, 1                  |    7.9 |    5.6 |   45.1 |   262 |    80 |  13 |  10 |  77 |
| burger ham patty Example Brand, 1     |   18.8 |   27.1 |    0.0 |   319 |   116 |  41 |  59 |   0 |
| cheese Hochland Cheeseburger, 1 slice |    2.7 |    4.4 |    0.8 |    53 |    19 |  34 |  56 |  10 |
| red onion rings, 10g                  |    0.1 |    0.0 |    0.8 |     4 |    10 |  11 |   0 |  89 |
| pink tomato, 1 slice                  |    0.3 |    0.1 |    1.2 |     6 |    32 |  19 |   6 |  75 |
| mustard sauce Heinz, 2 teaspoons      |    0.3 |    6.8 |    3.6 |    78 |    20 |   3 |  64 |  34 |
| pickle, 1/4                           |    0.0 |    0.0 |    0.3 |     1 |    10 |   0 |   0 | 100 |
#+TBLFM: @2$2=vsum(@<<<$0..@>$0);%.1f::@2$3=vsum(@<<<$0..@>$0);%.1f::@2$4=vsum(@<<<$0..@>$0);%.1f::@2$5=vsum(@<<<$0..@>$0);%.0f::@2$6=vsum(@<<<$0..@>$0);%.0f::$7=100*$2/($2+$3+$4);%.0f::$8=100*$3/($2+$3+$4);%.0f::$9=100*$4/($2+$3+$4);%.0f
#+END:
***** bun Example Brand, 1
:PROPERTIES:
:mass:     80
:protein:  7.92
:fat:      5.6
:carbs:    45.12
:kcal:     262.4
:END:
***** burger ham patty Example Brand, 1
:PROPERTIES:
:mass:     116
:protein:  18.792
:fat:      27.144
:carbs:    0.
:kcal:     319.464
:END:
***** cheese Hochland Cheeseburger, 1 slice
:PROPERTIES:
:mass:     18.75
:protein:  2.7375
:fat:      4.36875
:carbs:    0.80625
:kcal:     53.0625
:END:
***** red onion rings, 10g
:PROPERTIES:
:mass:     10
:protein:  0.12
:fat:      0.02
:carbs:    0.79
:kcal:     3.7
:END:
***** pink tomato, 1 slice
:PROPERTIES:
:mass:     32
:protein:  0.288
:fat:      0.064
:carbs:    1.248
:kcal:     5.76
:END:
***** mustard sauce Heinz, 2 teaspoons
:PROPERTIES:
:mass:     20
:protein:  0.3
:fat:      6.8
:carbs:    3.6
:kcal:     78.
:END:
***** pickle, 1/4
:PROPERTIES:
:mass:     10.16
:protein:  0.
:fat:      0.
:carbs:    0.3048
:kcal:     1.2192
:END:
**** chicken fingers KFC, 2
:PROPERTIES:
:mass:     56
:protein:  13.72
:fat:      8.512
:carbs:    7.672
:kcal:     162.176
:END:
**** apple Granny Smith, 1
:PROPERTIES:
:mass:     150
:protein:  0.6
:fat:      0.6
:carbs:    14.55
:kcal:     72.
:END:
**** hard-boiled eggs, 2
:PROPERTIES:
:mass:     120
:protein:  15.48
:fat:      13.92
:carbs:    0.96
:kcal:     192.
:END:
* locals
